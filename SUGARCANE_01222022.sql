-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2022 at 04:58 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sugarcane`
--

-- --------------------------------------------------------

--
-- Table structure for table `coordinates`
--

CREATE TABLE `coordinates` (
  `id` int(11) NOT NULL,
  `city` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `crops`
--

CREATE TABLE `crops` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `number_of_crops` int(11) NOT NULL,
  `date_to_be_harvested` datetime NOT NULL,
  `price_per_crop` decimal(12,3) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `crops`
--

INSERT INTO `crops` (`id`, `area_id`, `number_of_crops`, `date_to_be_harvested`, `price_per_crop`, `date_added`, `status`) VALUES
(2, 7, 300, '2021-12-08 00:00:00', '10.000', '2021-12-08 02:07:02', 2),
(3, 1, 1000, '2021-12-08 00:00:00', '5.000', '2021-12-08 15:54:28', 1),
(6, 22, 200, '2021-12-13 00:00:00', '5.000', '2021-12-14 09:15:49', 1),
(7, 23, 300, '2022-01-31 00:00:00', '5.000', '2021-12-14 09:51:21', 1),
(8, 26, 500, '2021-12-13 00:00:00', '35.000', '2021-12-14 11:24:26', 2);

-- --------------------------------------------------------

--
-- Table structure for table `crop_area`
--

CREATE TABLE `crop_area` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL,
  `coordinates` text NOT NULL,
  `map_key` varchar(5) NOT NULL,
  `hectare` decimal(12,3) NOT NULL,
  `open_close` int(1) NOT NULL DEFAULT 0 COMMENT '0=open, 1=close'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `crop_area`
--

INSERT INTO `crop_area` (`id`, `farm_id`, `coordinates`, `map_key`, `hectare`, `open_close`) VALUES
(22, 8, '101,129,12,230,114,316,238,202,213,98', 'ZX', '1.000', 1),
(23, 8, '93,423,323,415,400,561,235,557,133,552,79,523', 'DI', '1.000', 1),
(25, 8, '351,99,357,426,901,426,893,87', 'KT', '3.000', 0),
(26, 10, '108,177,109,316,332,323,331,218,268,215,244,208,231,208,205,177', 'PD', '3.000', 0),
(27, 10, '386,144,386,237,538,279,596,275,595,111,437,126', 'EA', '2.000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `farms`
--

CREATE TABLE `farms` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `farm_name` varchar(255) NOT NULL,
  `farm_desc` text NOT NULL,
  `farm_address` text NOT NULL,
  `hectare` decimal(12,3) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `farms`
--

INSERT INTO `farms` (`id`, `company_id`, `farm_name`, `farm_desc`, `farm_address`, `hectare`, `latitude`, `longitude`, `status`) VALUES
(8, 6, 'Farm with measure', 'test', 'C7XF+PM San Carlos City, Negros Occidental, Philippines', '10.000', '10.44929795111211', '123.2741932074219', 0),
(9, 6, 'Farm with Measure 2', 'test', 'H933+8G Calatrava, Negros Occidental, Philippines', '15.000', '10.553270229022006', '123.35384408632815', 0),
(10, 6, 'Sample Farm', 'Sample Farm', 'GCR2+PF San Carlos City, Negros Occidental, Philippines', '20.000', '10.541794489123829', '123.40122262636721', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migrations` text NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migrations`, `batch`) VALUES
(1, '20210408051901_create_password_resets_table', 1),
(2, '20210408051901_create_roles_table', 1),
(3, '20210408051901_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `company_name` varchar(200) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `role_id` varchar(3) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL,
  `color` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `company_name`, `username`, `password`, `role_id`, `remember_token`, `updated_at`, `created_at`, `status`, `color`) VALUES
(1, 'dams2020@gmail.com', 'test test', 'admin@gmail.com', '$2y$10$snDzcL3mhsZzhgGsd48VxOobI9hPc450SORZ.5JYVoyzhk6PN82Dq', 'A', NULL, '2021-10-14 22:25:09', '2021-10-14 22:25:09', 1, ''),
(4, 'ibayonabel@gmail.com', 'Sample Farm 1', 'ibayonabel@gmail.com', '$2y$10$2fQDVwglpPRwsImktGlCiew9AhqH92XkAqs49/t4pWh2tzVZ0121G', 'C', NULL, '2021-10-27 21:50:58', '2021-10-27 21:50:58', 1, '#84b717'),
(5, 'abelfullbusterbayon@gmail.com', 'Sample Farm 2', 'abelfullbusterbayon@gmail.com', '$2y$10$4ox4tXWOnsbywLXL46m45O.70f5t9.i7rcv3g86TbImVWHHkb8k26', 'C', NULL, '2021-10-27 22:07:52', '2021-10-27 22:07:52', 0, '#51b37d'),
(6, 'bjochelle@gmail.com', 'Sample Farm 3', 'bjochelle@gmail.com', '$2y$10$snDzcL3mhsZzhgGsd48VxOobI9hPc450SORZ.5JYVoyzhk6PN82Dq', 'C', NULL, '2021-10-27 22:08:18', '2021-10-27 22:08:18', 1, '#338443'),
(7, 'aragon.julieann009@gmail.com', 'Test Company 1', 'aragon.julieann009@gmail.com', '$2y$10$6DLnk4fuTiLF3NW3942ttu7WbRXg1wwHVN884P5OK8XIKQaHmTAFu', 'C', NULL, '2021-11-03 10:20:24', '2021-11-03 10:20:24', 1, '#a3d585');

-- --------------------------------------------------------

--
-- Table structure for table `user_uploads`
--

CREATE TABLE `user_uploads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `filetype` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` text NOT NULL,
  `iconsize` text NOT NULL,
  `upload_category` varchar(5) NOT NULL,
  `farm_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_uploads`
--

INSERT INTO `user_uploads` (`id`, `user_id`, `slug`, `filetype`, `filename`, `filesize`, `iconsize`, `upload_category`, `farm_id`, `created_at`) VALUES
(33, 6, 'public/assets/uploads/QPEU20211102112418.jpg', 'JPG', 'bp', '0.151339', '100%', 'F', 3, '2021-11-02 15:24:18'),
(34, 7, 'public/assets/uploads/O6FK20211103063232.jpg', 'JPG', 'bp', '0.151339', '100%', 'F', 5, '2021-11-03 10:32:32'),
(35, 6, 'public/assets/uploads/8WWQ20211104123108.jpg', 'JPG', 'banner1', '0.105047', '100%', 'F', 4, '2021-11-03 16:31:08'),
(37, 6, 'public/assets/uploads/5NJN20211207064838.jpg', 'JPG', 'banner', '0.038277', '100%', 'F', 7, '2021-12-07 10:48:38'),
(38, 6, 'public/assets/uploads/FFTX20211213073353.jpg', 'JPG', 'banner_2', '0.123776', '100%', 'F', 8, '2021-12-13 11:33:53'),
(39, 6, 'public/assets/uploads/9QD720211214061404.jpg', 'JPG', 'banner', '0.038277', '100%', 'F', 9, '2021-12-14 10:14:04'),
(40, 6, 'public/assets/uploads/LS7D20211214072121.jpg', 'JPG', '2513b22e1e882eaf3e564cde94c11593', '0.060413', '100%', 'F', 10, '2021-12-14 11:21:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coordinates`
--
ALTER TABLE `coordinates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crops`
--
ALTER TABLE `crops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crop_area`
--
ALTER TABLE `crop_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farms`
--
ALTER TABLE `farms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_uploads`
--
ALTER TABLE `user_uploads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coordinates`
--
ALTER TABLE `coordinates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crops`
--
ALTER TABLE `crops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `crop_area`
--
ALTER TABLE `crop_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `farms`
--
ALTER TABLE `farms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_uploads`
--
ALTER TABLE `user_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
