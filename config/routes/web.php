<?php

/**
 * --------------------------------------------------------------------------
 * Routes
 * --------------------------------------------------------------------------
 * 
 * Here is where you can register routes for your application.
 * Now create something great!
 * 
 */

use App\Core\Routing\Route;

Route::get('/', function () {
    $pageTitle = "Welcome";
    return view('/welcome', compact('pageTitle'));
});

Route::get('/home', ['WelcomeController@home', ['auth']]);
Route::post('/update-notif', ['WelcomeController@update']);

Route::group(["prefix" => "map", "middleware" => ["auth"]], function () {
    Route::get('/', ['MapController@index']);

    Route::post('/get-coords', ['MapController@coords']);
});

Route::group(["prefix" => "company", "middleware" => ["auth"]], function () {
    Route::get('/', ['CompanyController@index']);

    Route::post('/search-data', ['CompanyController@search']);
    Route::post('/change-status', ['CompanyController@changeStatus']);
});

Route::group(["prefix" => "calendar", "middleware" => ["auth"]], function () {
    Route::get('/', ['CalendarController@index']);

    Route::post('/active-cropping-data', ['CalendarController@croppingData']);

});

Route::group(["prefix" => "farms", "middleware" => ["auth"]], function () {
    Route::get('/', ['FarmController@index']);
    Route::get('/new-farm', ['FarmController@newFarm']);
    Route::get('/view-farm/{id}', ['FarmController@view']);
    Route::get('/new-canvas/{id}', ['FarmController@new']);

    Route::post('/save-farm', ['FarmController@store']);
    Route::post('/uploadFile/{id}', ['FarmController@storeFile']);

    Route::post('/add-area', ['FarmController@storeArea']);
    Route::post('/add-crop', ['FarmController@storeCrop']);
    Route::post('/crop-details', ['FarmController@cropDetails']);
    Route::post('/update-crop', ['FarmController@cropUpdate']);
    Route::post('/harvest-crop', ['FarmController@harvestCrop']);
    Route::post('/add-hectare-area', ['FarmController@storeMeasureArea']);
    Route::post('/finish-harvest', ['FarmController@finishHarvest']);
    Route::post('/open_area_for_new', ['FarmController@openAreafornewPlant']);
    Route::post('/crop-confirm', ['FarmController@countCropping']);
    Route::post('/history-list', ['FarmController@historyList']);
});

Route::group(["prefix" => "reports", "middleware" => ["auth"]], function () {
    Route::get('/area-report', ['ReportController@areaReport']);
    Route::get('/expenses', ['ReportController@expensesReport']);
    Route::get('/crops', ['ReportController@cropsReport']);

    Route::post('/area-report-pie', ['ReportController@areaReportPie']);
    Route::post('/expenses-report-column', ['ReportController@expensesReportColumn']);
    Route::post('/crops-report-column', ['ReportController@cropsReportColumn']);
});
