<?php
use App\Core\Auth;
function numberOfCompanies()
{
    $count = DB()->select("count(*) as c", "users", "role_id = 'C'")->get();

    return $count['c']*1;
}

function numberOfFarms()
{
    $count = DB()->select("count(*) as c", "farms")->get();

    return $count['c']*1;
}

function array_months(){
    $array = [
        "Jan" => "01",
        "Feb" => "02",
        "Mar" => "03",
        "Apr" => "04",
        "May" => "05",
        "Jun" => "06",
        "Jul" => "07",
        "Aug" => "08",
        "Sept" => "09",
        "Oct" => 10,
        "Nov" => 11,
        "Dec" => 12
    ];

    return $array;
}
function checkIfImage($filetype)
{
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");

    if (in_array($filetype, $image_icons_array)) {
        $response = true;
    } else {
        $response = false;
    }

    return $response;
}

function getImageView($filetype, $path)
{
    $logo_path = public_url("/assets/sprnva/file_extension_icon/");
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
    $file_icons_array = array("XLS", "DOCX", "CSV", "TXT", "ZIP", "EXE", "XLSX", "PPT", "PPTX", "PDF");

    if (in_array($filetype, $image_icons_array)) {
        $icon = public_url("/../" . $path);
    } else {
        if (in_array($filetype, $file_icons_array)) {
            $icon = $logo_path . $filetype . '.png';
        } else {
            $icon = $logo_path . 'FILE.png';
        }
    }

    return $icon;
}

function getAvatar()
{
    $me = Auth::user('id');
    $avatar = DB()->select("slug", "user_uploads", "user_id = '$me' AND upload_category = 'A'")->get();

    return (empty($avatar['slug']))?"":$avatar['slug'];
}

function getBlueprint($id)
{
    $me = Auth::user('id');
    $avatar = DB()->select("slug", "user_uploads", "user_id = '$me' AND upload_category = 'F' AND farm_id = '$id'")->get();

    return (empty($avatar['slug']))?"":$avatar['slug'];
}

function getBlueprintFileType($id)
{
    $me = Auth::user('id');
    $avatar = DB()->select("filetype", "user_uploads", "user_id = '$me' AND upload_category = 'F' AND farm_id = '$id'")->get();

    return (empty($avatar['filetype']))?'':$avatar['filetype'];
}

function getFileType()
{
    $me = Auth::user('id');
    $avatar = DB()->select("filetype", "user_uploads", "user_id = '$me'")->get();

    return (empty($avatar['filetype']))?'':$avatar['filetype'];
}

function getAddress($latitude,$longitude){
    if(!empty($latitude) && !empty($longitude)){
        //Send request and receive json data by address
        $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=10.6344115,122.952217&sensor=true&key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0'); 
        $output = json_decode($geocodeFromLatLong);
        $status = $output->status;
        //Get address from json data
        $address = ($status=="OK")?$output->results[2]->formatted_address:'';
        //Return address of the given latitude and longitude
        if(!empty($address)){
            return $address;
        }else{
            return $address;
        }
    }else{
        return false;   
    }
}

function FarmCounter($company_id){

    $farm = DB()->select("count(*) as c", "farms", "company_id = '$company_id'")->get();

    return $farm['c'];

}

function getRandonChars()
{
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $length = 2;
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}








