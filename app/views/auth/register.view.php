<?php

use App\Core\App;
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='icon' href='<?= public_url('/favicon.ico') ?>' type='image/ico' />
    <title>
        <?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name']; ?>
    </title>

    <link rel="stylesheet" href="<?= public_url('/assets/sprnva/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/themify-icons/themify-icons.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/icofont/css/icofont.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/feather/css/feather.css') ?>">
    
    
    
    <link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/pages/j-pro/css/demo.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/pages/j-pro/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/pages/j-pro/css/j-pro-modern.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/css/style.css') ?>">

    <style>
        @font-face {
            font-family: Nunito;
            src: url("<?= public_url('/assets/sprnva/fonts/Nunito-Bold.ttf') ?>");
        }

        body {
            font-weight: 300;
            font-family: Nunito;
            color: #26425f;
            background: #eef1f4;
        }

        .icofont {
            font-family: 'IcoFont' !important;
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 2;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

    </style>

    <script src="<?= public_url('/assets/sprnva/js/jquery-3.6.0.min.js') ?>"></script>
    <script src="<?= public_url('/assets/sprnva/js/popper.min.js') ?>"></script>
    <script src="<?= public_url('/assets/sprnva/js/bootstrap.min.js') ?>"></script>

    <script type="text/javascript" src="<?= public_url('/assets/adminty/assets/pages/j-pro/js/jquery.ui.min.js') ?>"></script>
    <script type="text/javascript" src="<?=public_url('/assets/adminty/assets/pages/j-pro/js/jquery.maskedinput.min.js')?>"></script>
    <script type="text/javascript" src="<?=public_url('/assets/adminty/assets/pages/j-pro/js/jquery.j-pro.js')?>"></script>

    <script>
        const base_url = "<?= App::get('base_url') ?>";
    </script>
</head>

<body>
<div class="pcoded-content">
    <div class="pcoded-inner-content">

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
            <div class="card">
            <div class="card-header">
            <h4>Please fill all provided fields</h4>
            </div>
            <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="<?= route('/register') ?>" method="post" class="j-pro" id="j-pro" novalidate>
                            <?= alert_msg(); ?>
                            <div class="j-content">
                                <div>
                                    <label class="j-label">Company Name</label>
                                    <div class="j-unit">
                                        <div class="j-input">
                                            <label class="j-icon-right" for="name">
                                                <i class="icofont icofont-ui-user"></i>
                                            </label>
                                            <input type="text" autocomplete='off' id="name" name="name">
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div>
                                        <label class="j-label">Email</label>
                                    </div>
                                    <div class="j-unit">
                                        <div class="j-input">
                                            <label class="j-icon-right" for="email">
                                            <i class="icofont icofont-envelope"></i>
                                            </label>
                                            <input type="email" autocomplete='off' id="email" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div>
                                    <label class="j-label ">Password</label>
                                    </div>
                                    <div class="j-unit">
                                        <div class="j-input">
                                        <label class="j-icon-right" for="login">
                                        <i class="icofont icofont-lock"></i>
                                        </label>
                                        <input type="password" autocomplete='off' id="password" name="password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary btn-sm btn-round"><i class="icofont icofont-ui-check"></i> Register</button>

                                <a href="<?= route('/login') ?>"><i class="icofont icofont-login"></i> Back to Login</a> | <a href="<?= route('/') ?>"><i class="icofont icofont-home"></i> Back to Homepage</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>
    </div>
</div>
</body>
<script>
</script>
</html>