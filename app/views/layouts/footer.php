</div>
</body>
<script>
$(document).ready( function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('#datepicker1').datepicker();
});
    function gotoPage(id, type, trans){
      $.post(base_url+"/update-notif", {
        id: id
      }, function(res){
        if(type == 0){
          var routes = '<?=route("/patientDelivery/view-details")?>';
          window.location = routes + '/' +trans;
        }else{
          var routes = '<?=route("/patientAppointments/view")?>';
          window.location = routes + '/' +trans;
        }
      })
    }
    function delete_success(message){
        $.confirm({
            icon: 'feather icon-check-circle text-green',
            title: 'Success!',
            type: 'green',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function update_success(message){
        $.confirm({
            icon: 'feather icon-check-circle text-green',
            title: 'Success!',
            type: 'green',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function add_success(message){
        $.confirm({
            icon: 'feather icon-check-circle text-green',
            title: 'Success!',
            type: 'green',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function failed_query(){
        $.confirm({
            icon: 'feather icon-x text-red',
            title: 'Error Encountered!',
            type: 'red',
            content: 'There was an error while saving the data',
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function warning_alert(message){
        $.confirm({
            icon: 'feather icon-alert-triangle text-orange',
            title: 'Warning!',
            type: 'orange',
            content: message,
            buttons:{
              Okay: function(){
                
              }
            }
        });
    }
</script>
</html>