<?php

use App\Core\App;
use App\Core\Auth;
?>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='icon' href='<?= public_url('/storage/images/sugarcaneicon_icon.ico') ?>' type='image/ico' />
	<title>
		<?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name'] ?>
	</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/icon/feather/css/feather.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/icofont/css/icofont.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/pages/data-table/css/buttons.dataTables.min.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" type="text/css">

	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/bootstrap-datepicker/css/bootstrap-datepicker3.standalone.min.css') ?>" type="text/css">

	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/css/style.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/css/jquery.mCustomScrollbar.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/jquery-confirm/jquery-confirm.min.css') ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
	
	<link href='<?= public_url('/assets/adminty/fullcalendar/fullcalendar.min.css') ?>' rel='stylesheet' />
	<link href='<?= public_url('/assets/adminty/fullcalendar/fullcalendar.print.min.css') ?>' media="print" rel='stylesheet' />
	<link href='<?= public_url('/assets/adminty/daterangepicker/daterangepicker-bs3.css') ?>' rel='stylesheet' />
	<link href='<?= public_url('/assets/adminty/bower_components/switchery/css/switchery.min.css') ?>' rel='stylesheet' />
	<style>
	html, body {
		width:  100%;
		height: 100%;
		margin: 0;
		}
	</style>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/jquery-ui/js/jquery-ui.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/popper.js/js/popper.min.js') ?>"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	
	<script src="<?= public_url('/assets/adminty/bower_components/jquery-slimscroll/js/jquery.slimscroll.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/modernizr/js/modernizr.js') ?>"></script>

	<!-- datatables -->
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datedropper/js/datedropper.min.js') ?>"></script>

	<script src="<?= public_url('/assets/adminty/assets/js/jquery.mCustomScrollbar.concat.min.js') ?>"></script>
	<!-- <script src="<?= public_url('/assets/adminty/assets/js/SmoothScroll.js') ?>"></script> -->
	<script src="<?= public_url('/assets/adminty/assets/js/pcoded.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/vartical-layout.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/script.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/pcoded.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/jquery-confirm/jquery-confirm.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/fullcalendar/moment.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/fullcalendar/fullcalendar.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/switchery/js/switchery.min.js') ?>"></script>

	<script src="<?= public_url('/assets/adminty/mapper/mapper.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/mapper/maputil.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/mapper/jquery.canvasAreaDraw.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/mapper/imagemapster.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/highchart/highcharts1.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/highchart/highcharts-more.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/highchart/exporting.js') ?>"></script>
	<?php require_once __DIR__ . '/filepond.php'; ?>
	<script>
		const base_url = "<?= App::get('base_url') ?>";
	</script>
	<?php
		// this will auto include filepond css/js when adding filepond in public/assets
		if (file_exists('public/assets/filepond')) {
			require_once 'public/assets/filepond/filepond.php';
		}
	?>
</head>

<body>
	<!-- Pre-loader start -->
	<div class="theme-loader">
		<div class="ball-scale">
			<div class='contain'>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Pre-loader end -->

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">

			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">

					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="#!">
							<i class="feather icon-menu"></i>
						</a>
						<a href="<?= route('/') ?>">
							<h5></h5>
						</a>
						<a class="mobile-options">
							<i class="feather icon-more-horizontal"></i>
						</a>
					</div>

					<div class="navbar-container container-fluid">
						<ul class="nav-left">
							<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="feather icon-maximize full-screen"></i>
								</a>
							</li>
						</ul>
						<ul class="nav-right">
							<li class="header-notification">
								<div class="dropdown-primary dropdown">
									<div class="dropdown-toggle" data-toggle="dropdown">
										<i class="feather icon-bell"></i>
										<span class="badge bg-c-pink">5</span>
									</div>
									<ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
										<li>
											<h6>Notifications</h6>
										</li>
										
											<li>
												<div class="media">
													<div class="media-body">
														<h5 class="notification-user"></h5>
														<p class="notification-msg"></p>
														<span class="notification-time"></span>
													</div>
												</div>
											</li>
									
									</ul>
								</div>
							</li>
							<li class="user-profile header-notification">
								<div class="dropdown-primary dropdown">
									<div class="dropdown-toggle" data-toggle="dropdown">
								
										<?php if(empty(getAvatar())){ ?>
											
											<img src="<?= public_url('/storage/images/avatar.png') ?>" class="img-radius" alt="User-Profile-Image">
										<?php } else { ?>
											
											<img src="<?= getImageView(getFileType(), getAvatar()) ?>" class="img-radius" alt="User-Profile-Image" style='object-fit: cover'>
										<?php } ?>
										<span><?= Auth::user('company_name') ?></span>
										
										<i class="feather icon-chevron-down"></i>
									</div>
									<ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
										<!-- <li>
											<a href="#!">
												<i class="feather icon-settings"></i> Settings
											</a>
										</li> -->
										<li>
											<a href="<?= route('/profile') ?>">
												<i class="feather icon-user"></i> Profile
											</a>
										</li>
										<li>
											<a href="<?= route('/logout') ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
												<i class="feather icon-log-out"></i> Logout
											</a>

											<form id="logout-form" action="<?= route('/logout') ?>" method="POST" style="display:none;">
												<?= csrf() ?>
											</form>
										</li>
									</ul>

								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<nav class="pcoded-navbar">
						<div class="pcoded-inner-navbar main-menu">
							<div class="pcoded-navigatio-lavel">Navigation</div>
							<ul class="pcoded-item pcoded-left-item">
								<li class="">
									<a href="<?= route('/home') ?>">
										<span class="pcoded-micon"><i class="feather icon-grid"></i></span>
										<span class="pcoded-mtext">Dashboard</span>
									</a>
								</li>
								<?php if(Auth::user('role_id') == 'A') { ?>
								<li class="">
									<a href="<?= route('/company') ?>">
										<span class="pcoded-micon"><i class="icofont icofont-company"></i></span>
										<span class="pcoded-mtext">Companies</span>
									</a>
								</li>
								<li class="">
									<a href="<?= route('/map') ?>">
										<span class="pcoded-micon"><i class="feather icon-compass"></i></span>
										<span class="pcoded-mtext">San Carlos Map</span>
									</a>
								</li>
								<!-- <li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                        <span class="pcoded-mtext">Hospitals</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="<?= route('/pending') ?>">
                                                <span class="pcoded-mtext">Pending Applications</span>&nbsp;
												<span class="pcoded-badge label label-danger "></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="<?= route('/active') ?>">
                                                <span class="pcoded-mtext">Active Hospitals/Clinics</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?= route('/inactive') ?>">
                                                <span class="pcoded-mtext">Inactive Hospitals/Clinics</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li> -->
								<?php } ?>
								<?php if(Auth::user('role_id') == 'C') { ?>
								<li class="">
									<a href="<?= route('/farms') ?>">
										<span class="pcoded-micon"><i class="feather icon-compass"></i></span>
										<span class="pcoded-mtext">Farms</span>
									</a>
								</li>
								<li class="">
									<a href="<?= route('/calendar') ?>">
										<span class="pcoded-micon"><i class="feather icon-calendar"></i></span>
										<span class="pcoded-mtext">Cropping Calendar</span>
									</a>
								</li>
								<li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-bar-chart"></i></span>
                                        <span class="pcoded-mtext">Reports</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="<?= route('/reports/area-report') ?>">
                                                <span class="pcoded-mtext">Statistics</span>
                                            </a>
                                        </li>
                                        <!-- <li class="">
                                            <a href="<?= route('/reports/expenses') ?>">
                                                <span class="pcoded-mtext">Expenses</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?= route('/reports/crops') ?>">
                                                <span class="pcoded-mtext">Crops</span>
                                            </a>
                                        </li> -->
                                    </ul>
                                </li>
								<?php } ?>
							</ul>
						</div>
					</nav>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<!-- Page-header start -->
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4><?=($pageTitle == "Home")?"":$pageTitle?></h4>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- Page-header end -->
									<div class="page-body">