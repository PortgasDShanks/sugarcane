<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';

?>

<style>
#Hospitalmap {
    height: 600px; 
    width: 100%;  
}
.ch-padd-hover:hover .show-onhover {
    display: block;
}

.show-onhover {
    display: none;
    float: right;
}
.label {
    border-radius: 4px;
    font-size: 100%;
    padding: 4px 7px;
    margin-right: 5px;
    font-weight: 400;
    color: #fff !important;
}
.label-success {
    background: -webkit-gradient(linear, left top, right top, from(#0ac282), to(#0df3a3));
    background: linear-gradient(to right, #0ac282, #09c383);
}
</style>
<div class="row">
    <div class='col-sm-12'>
    <div class="card">
        <div class="card-block">
            <div class="row">
                
                <div class='col-sm-12'>
                    <form method='POST' id='searchforme'>
                    <div class="input-group input-group-info">
                        
                            <input autocomplete='off' type="text" class="form-control" placeholder="Search for Farms! Just input the name of the Farm, Ex. Sugarcane Farm" id='searchval' name='searchval'>
                            <span class="input-group-addon" id='searchBtn' onclick='search_for_doctor()'>
                                <i class="feather icon-search"></i>
                            </span>
                        
                    </div>
                    </form>
                </div>
                
               <div class='col-sm-12 row' id='paste_content'></div>
            </div>
        </div>
    </div>
    </div>
</div>
<script>
$(document).ready( function(){
    search_for_doctor();
    $("#searchforme").on('submit', function(e){
        e.preventDefault();
        $("#searchBtn").prop("disabled", true);
        $("#searchBtn").html("<span class='feather icon-aperture rotate-refresh'></span>");
        $("#paste_content").html("<div class='col-sm-12 d-flex flex-column align-items-center justify-content-center'><span style='font-size: 5em;' class='feather icon-aperture rotate-refresh '></span></div>");
        const url = base_url+"/company/search-data";
        const data = $(this).serialize();
        $.post(url, data, function(res){
            $("#paste_content").html(res);

            $("#searchBtn").prop("disabled", false);
            $("#searchBtn").html("<span class='feather icon-search'></span>");
        });
    });
});
function search_for_doctor(){
    const searchval = $("#searchval").val();
    $("#searchBtn").prop("disabled", true);
    $("#searchBtn").html("<span class='feather icon-aperture rotate-refresh'></span>");
    $("#paste_content").html("<div class='col-sm-12 d-flex flex-column align-items-center justify-content-center'><span style='font-size: 5em;' class='feather icon-aperture rotate-refresh '></span></div>");
    $.post(base_url+"/company/search-data", {
        searchval: searchval
    }, function(data){
        $("#paste_content").html(data);

        $("#searchBtn").prop("disabled", false);
        $("#searchBtn").html("<span class='feather icon-search'></span>");
    });
        
}
function showOptions(id, status, name){
    if(status == 0){
        $.confirm({
            icon: 'feather icon-info',
            title: "Confirmation",
            type: 'blue',
            closeIcon: true,
            closeIconClass: 'iconfont icon-close',
            columnClass: 'col-md-6 col-md-offset-3',
            content: '<strong>' +name+ '</strong> is currently on Pending Application status. Would you like to approve or cancel this application?',
            buttons: {
    
                Approve: {
                    text: 'Approve Application',
                    btnClass: 'btn-info',
                    action: function () {
                        applicationStatus(id, 1);
                    }
                },
                Cancel:{
                    text: 'Cancel Application',
                    btnClass: 'btn-danger',
                    action: function(){
                        applicationStatus(id, 3);
                    }
                },
                close: {
                action: function () {
                    $.alert("Action Aborted!");
                }
            }
            }
        });
    }else if(status == 1){
        $.confirm({
            icon: 'feather icon-info',
            title: "Confirmation",
            type: 'blue',
            content: '<strong>' +name+ '</strong> is currently in ACTIVE status. Would you like to set this company to inactive?',
            buttons: {
    
                Approve: {
                    text: 'Yes, Set to Inactive',
                    btnClass: 'btn-warning',
                    action: function () {
                        applicationStatus(id, 2);
                    }
                },
                close: {
                action: function () {
                    $.alert("Action Aborted!");
                }
            }
            }
        });
    }else if(status == 2){
        $.confirm({
            icon: 'feather icon-info',
            title: "Confirmation",
            type: 'blue',
            content: '<strong>' +name+ '</strong> is currently in INACTIVE status. Would you like to set back this company to active?',
            buttons: {
    
                Approve: {
                    text: 'Yes, Set back to Active',
                    btnClass: 'btn-success',
                    action: function () {
                        applicationStatus(id, 4);
                    }
                },
                close: {
                action: function () {
                    $.alert("Action Aborted!");
                }
            }
            }
        });
    }else{
        $.confirm({
            icon: 'feather icon-info',
            title: "Confirmation",
            type: 'blue',
            content: '<strong>' +name+ '</strong> application was cancelled'
        });
    }
   
}

function applicationStatus(id, status){
    var stts = (status == 1) ? "Approved Applications" : ((status == 2) ? "Inactive Companies" : "Cancelled Applications")
    $.post(base_url+"/company/change-status", {
        id: id,
        status: status
    }, function(res){
        //alert(res);
        //if(res > 0){
            update_success("Successfully changed to "+ stts);
        // }else{
        //     failed_query();
        // }
    });
}


</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>

