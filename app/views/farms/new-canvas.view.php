<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';

?>

<style>
/* img#gmipam_0_image {
    width: 100% !important;
    height: 500px!important;
    padding: 10px;
}

canvas {
    width: 100% !important;
    height: auto;
    object-fit: cover;
} */

canvas{
  /* width:100%;
  height:100%; */
 
  background-repeat: no-repeat;
}

</style>
<div class="row">
    <div class='col-sm-12'>
    <div class="card">
        <div class="card-block">
                <div class="row">
                    <div class='col-sm-12'>
                        <button  style='float: right' class='btn btn-sm btn-primary btn-round' onclick="saveCanvas()"><span></span> Save Canvas</button>

                        <button  style='float: right' class='btn btn-sm btn-primary btn-round' onclick="window.location='<?=route('/farms/view-farm', $id)?>'"><span></span> Go Back</button>
                    </div>

                    <?php
                        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

                        $url = $protocol . $_SERVER['HTTP_HOST'] ."/training-1/".$slug;
                    
                    ?>
                    <div id="main" class="container">
                   <div class='span12' style='overflow: auto'>
                            <textarea 
                                rows=3 
                                style='display:none' 
                                id='coords1' 
                                name="coords1" 
                                class="canvas-area input-xxlarge" 
                                disabled 
                                placeholder="Shape Coordinates"
                                data-image-url="<?=$url?>" 
                                >
                            </textarea>
                          
                   </div>
</div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<?php require __DIR__ . '/add-measurement-modal.php'; ?>
<script>
$(document).ready( function(){
    var canvas = document.querySelector('canvas');
    fitToContainer(canvas);
    var farmID = '<?=$id?>';
    $("#addMeasure").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/farms/add-hectare-area";
        var data = $(this).serialize();

        $.post(url, data, function(res){
            if(res > 0){
                $.confirm({
                    icon: 'feather icon-check-circle text-green',
                    title: 'Success!',
                    type: 'green',
                    content: "New Area has been successfully Added!",
                    buttons:{
                    Okay: function(){
                        var routes = "<?=route('/farms/view-farm')?>";
                        window.location = routes+"/"+farmID;
                    }
                    }
                });
            }else{
                failed_query();
            }

            $("#addMeasureModal").modal('hide');
        });
    });

});

function saveCanvas(){
    var coords1 = $("#coords1").val();
    var farmID = '<?=$id?>';
    $.post(base_url+"/farms/add-area", {
        coords1: coords1,
        farmID: farmID
    }, function(res){
        $("#areaID").val(res);
        $("#addMeasureModal").modal();
        
    });
}
function fitToContainer(canvas){
    // Make it visually fill the positioned parent
    canvas.style.width ='1000px';
    canvas.style.height='700px';
    // ...then set the internal size to match
    canvas.width  = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
}
function show(){
    alert("Test")
}

function showModal(slug){
   

    
   //$("#coords1").attr("data-image-url", route+"/sugarcane/"+slug);

    $('#newCanvas').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}
function makeModal() {
    $('#make_modal').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}
function close_make_modal() {
    location.reload();
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>

