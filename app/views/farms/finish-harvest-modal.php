<form method="POST" id='finishHarvest'>
    <div class="modal fade" id="finishHarvestContent" tabindex="-1" role="dialog" aria-labelledby="finishHarvestContentLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="finishHarvestContentLabel"><span class='fa fa-check-circle'></span> Finish Harvesting!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label for=""></label>
                        <div class='form-group'>
                            <label for="">Harvest Type</label>
                            <select name="harvestType" id="harvestType" class='form-control'>
                                <option value="">&mdash; Please Select &mdash; </option>
                                <option value="3">Green Harvest</option>
                                <option value="4">Burnt Harvest</option>
                            </select>
                        </div>
                       
                            <input name="finishharvestCropiD" id='finishharvestCropiD' type='hidden'>
                            <input name="completeHarvest" id='completeHarvest' type='hidden'>
                            
                      
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save Changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>