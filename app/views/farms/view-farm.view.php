<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';

?>

<style>
img#gmipam_0_image {
    width: 100% !important;
    height: 500px!important;
    padding: 10px;
}

canvas {
    width: 100% !important;
    height: auto;
    object-fit: cover;
}
.modal-lg {
    max-width: 1360px;
}
</style>
<div class="row">
    <div class='col-sm-12'>
    <div class="card">
        <div class="card-block">
                <div class="row">
                    
                   <div class='col-sm-12'>
                        <?php if(!empty(getBlueprint($id))) { ?>
                            <div class='row'>
                                <div class='col-sm-4'>
                                    <button class='btn btn-sm btn-primary btn-round'><span></span> View Legends </button>
                                </div>
                                <div class='col-sm-8'>
                                    <button  style='float: right' class='btn btn-sm btn-primary btn-round' onclick="window.location='<?=route('/farms/new-canvas', $id)?>'"><span></span> Add New Area</button>

                                    <button  style='float: right' class='btn btn-sm btn-primary btn-round' onclick="window.location='<?=route('/farms')?>'"><span></span> Go Back</button>
                                </div>

                                
                                
                                <div class='col-sm-12' style='margin-top: 20px; overflow: auto'>
                                <img src="<?= getImageView(getBlueprintFileType($id), getBlueprint($id)) ?>" alt="Blueprint-Image" style='width:100%;height:auto;' usemap="#usa">
                                <map id="usa_image_map" name="usa">
                                    <?php
                                        foreach ($coord_area as $coord_areas) {
                                            $key = ($coord_areas['cstat'] == 0)?"TX":"CA";

                                            // $plantAdded = date_create($coord_areas["date_planted"]);

                                            // $DateHarvest = date_add($plantAdded,date_interval_create_from_date_string("10 months"));
                                    ?>
                                    <area onclick="selectOption('<?=$coord_areas['cstat']?>', '<?=$coord_areas['ccstat']?>', '<?=$coord_areas['areaid']?>', '<?=$coord_areas['cropid']?>', '<?=$coord_areas['date_planted']?>', '<?=$coord_areas['date_harvested']?>', '<?=$coord_areas['completed_status']?>')" href="#" state="<?=$coord_areas['map_key']?>" full="Texas" shape="poly" coords="<?=$coord_areas['coordinates']?>">
                                    <?php } ?>
                                    <!-- <area href="#" state="TX" shape="poly" coords="275,68,281,211,327,235,383,207,312,67"> -->
                                </map>
                                    
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class='col-sm-8'>
                                    <h6 style='color: red'>Note: Please resize your blueprint image approximately 1000x600 to have an accurate plotting of Area on your Map</h6>

                                </div>
                            <div class="d-flex flex-column align-items-center justify-content-center">
                                <h2 class="mb-0 text-muted welcome-msg">No Blueprint Detected</h2>
                                <a href="#" onclick='makeModal()' style='margin-top: 40px;'><span class='feather icon-upload-cloud'></span> Upload Blueprint</a>
                            </div>
                        
                        <?php } ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php require __DIR__ . '/upload-bp-modal.php'; ?>
    <?php require __DIR__ . '/new-crop-modal.php'; ?>
    <?php require __DIR__ . '/crop-details-modal.php'; ?>
    <?php require __DIR__ . '/harvest-crop-modal.php'; ?>
    <?php require __DIR__ . '/finish-harvest-modal.php'; ?>
    <?php require __DIR__ . '/area-open-modal.php'; ?>
    <?php require __DIR__ . '/view-cropping-history.php'; ?>
</div>
<!-- <script src="http://www.outsharked.com/scripts/jquery.imagemapster.js"></script> -->
<script>
$(document).ready( function(){
    $('#datepicker1').datepicker();
    $('#datepicker2').datepicker();
    $("#newCanvasSave").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/farms/add-area";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            $.confirm({
                icon: 'feather icon-check-circle text-green',
                title: 'Success!',
                type: 'green',
                content: "New Area has been successfully Added!",
                buttons:{
                Okay: function(){
                    window.location = "<?=route('/farms/view-farm')?>";
                }
                }
            });
        });
    });


    var basic_opts = {
        mapKey: 'state'
    };

    var initial_opts = $.extend({},basic_opts, 
        { 
            staticState: true,
            fill: false,
            stroke: true,
            strokeWidth: 1,
            strokeColor: 'ccc'
        });

    $('img').mapster(initial_opts)
    <?php
        $curDate = date('Y-m-d');
        $crops_highlight = DB()->selectLoop("*","`crop_area` as c","c.farm_id = '$id'")->get();
        foreach ($crops_highlight as $crops) {
            $crop_areas = DB()->select("*", "crops", "area_id = '$crops[id]' AND finished_status = 0")->get();
            if(!empty($crop_areas['id']) && $crops['open_close'] == 1){
                $datePlant = ($crop_areas["date_harvested"] == NULL || $crop_areas["date_harvested"] == '0000-00-00')?date_create($crop_areas["date_planted"]):date_create($crop_areas["date_harvested"]);

                $harvestDate = date_add($datePlant,date_interval_create_from_date_string("10 months"));

                $fillColor = ($crop_areas['status'] == 1 && $curDate > $harvestDate && $crop_areas['completed_status'] == 0)?"4DED30":(($curDate <= $harvestDate && $crop_areas['status'] == 1 && $crop_areas['completed_status'] == 0)?"A4DE02":(($crop_areas['status'] == 2 && $crop_areas['completed_status'] == 0)?"A4DE02":(($crop_areas['status'] == 3 && $crop_areas['completed_status'] == 0)?"0B6623":(($crop_areas['status'] == 4 && $crop_areas['completed_status'] == 0)?"6E0E0A":"F7E9D7"))));

                $strokeColor = ($crop_areas['status'] == 2)?"000000":"";;
                $strokeTrue = ($crop_areas['status'] == 2)?"true":"false";
              
            }else{
               $fillColor = "4DED30";
                $strokeColor = "000000";
               $strokeTrue = true;
            }
            
    ?>
        .mapster('set',true,'<?=$crops['map_key']?>', {
            fill: true,
            fillColor: '<?=$fillColor?>',
            stroke: <?=$strokeTrue?>,
            strokeWidth: 2,
            strokeColor: '<?=$strokeColor?>'
        })
    <?php } ?>
        
        .mapster('snapshot')
        .mapster('rebind',basic_opts);

    $("#saveCropPlant").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/farms/add-crop";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            $("#saveCrop").modal('hide');
            if(res > 0){
                add_success("New Crop has been successfully planted!");
            }else{
                failed_query();
            }
        });
    });

    $("#updateDetails").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/farms/update-crop";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            $("#cropDetails").modal('hide');
            if(res > 0){
                add_success("Crop Area has been successfully updated!");
            }else{
                failed_query();
            }
        });
    });

    $("#harvestCrop").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/farms/harvest-crop";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            $("#harvestCropContent").modal('hide');
            if(res > 0){
                add_success("This Area has started to harvest!");
            }else{
                failed_query();
            }
        });
    });

     $("#finishHarvest").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/farms/finish-harvest";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            $("#finishHarvestContent").modal('hide');
            if(res > 0){
                add_success("Harvest Successfully finished!");
            }else{
                failed_query();
            }
        });
     });

     $("#openAreaforNew").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/farms/open_area_for_new";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            $("#openAreaforNewContent").modal('hide');
            if(res > 0){
                add_success("Area has been successfully set to OPEN");
            }else{
                failed_query();
            }
        });
     });
});

function addMonths(date, months) {
    var d = date.getDate();
    date.setMonth(date.getMonth() + +months);
    if (date.getDate() != d) {
      date.setDate(0);
    }
    return date;
}

function selectOption(area_status, crop_status, areaid, cropid, date_planted, date_harvested, completed_status){
    // var d = new Date();

    // var month = d.getMonth()+1;
    // var day = d.getDate();

    // var output = d.getFullYear() + '-' +
    //     ((''+month).length<2 ? '0' : '') + month + '-' +
    //     ((''+day).length<2 ? '0' : '') + day;

    var output = '<?=date("Y-m-d")?>';

    var date_harvest = (date_harvested == null || date_harvested == '0000-00-00' || date_harvested == '')? addMonths(new Date(date_planted), 10).toString(): addMonths(new Date(date_harvested), 10).toString();

 

    var formatted_date = new Date(date_harvest);

    var dh_month = formatted_date.getMonth()+1;
    var dh_day = formatted_date.getDate();
    var dh_year = formatted_date.getFullYear();

    var fullDate = dh_year+"-0"+dh_month+"-"+dh_day;
 
   // (output < fullDate)?alert("Not Ready"):alert("ready");

    var conf = confirm("Would you like to view the cropping history");
    if(conf == true){
        $("#croppingHistory").modal();
        recepients_data(cropid);
    }else{
        if(area_status == 0){
            $("#areaID").val(areaid);
            $("#saveCrop").modal();
        }else if(area_status == 1 && crop_status == 1 && output < fullDate && completed_status == 0){
            
            $.post(base_url+"/farms/crop-details", {
                cropid: cropid
            }, function(res){
                var crop = JSON.parse(res);
                $("#totalHectareAreaNOC").val(crop.hectares);
                $("#datepicker1").val(crop.date_planted);
                $("#viewPPC").val(crop.price_per_crop);
                $("#cropID").val(cropid);

                $("#cropDetails").modal();
            });
        }else if(output >= fullDate && crop_status == 1 && completed_status == 0){
        
            $.post(base_url+"/farms/crop-details", {
                cropid: cropid
            }, function(res){
                var crop = JSON.parse(res);
                $("#totalHectareArea").val(crop.hectares);
                $("#datepicker2").val(crop.date_planted);
                $("#harvestDOH").val(crop.date_to_be_harvested);
                $("#harvestPPC").val(crop.price_per_crop);
                $("#harvestCropiD").val(cropid);
                $("#harvestAreaID").val(areaid);

                $("#harvestCropContent").modal();
            });
        }else if(crop_status == 2 && completed_status == 0){
            $.post(base_url+"/farms/crop-confirm", {
                cropid: cropid
            }, function(res){
                if(res >= 4){
                    var finish_confirm = confirm("You already have "+ res +" total of Cropping in this area. Would you like to finish the cropping of this area?");
                    if(finish_confirm == true){
                        //completedCropping(cropid);
                        var complete_stat = 1;
                        $("#finishHarvestContent").modal();
                        $("#finishharvestCropiD").val(cropid);
                        $("#completeHarvest").val(complete_stat); 
                    }else{
                        var complete_stat = 0;
                        $("#finishHarvestContent").modal();
                        $("#finishharvestCropiD").val(cropid);
                        $("#completeHarvest").val(complete_stat);  
                    }
                }else{
                    var complete_stat = 0;
                    $("#finishHarvestContent").modal();
                    $("#finishharvestCropiD").val(cropid);
                    $("#completeHarvest").val(complete_stat);  
                }

                
            });
           
        }else{
            // $.post(base_url+"/farms/get-latest-harvest-date", {
            //     cropid: cropid
            // }, function(res){

            // });

            $("#openAreaforNewContent").modal();
            //(crop_status == 3)?$("#area_status").html("GREEN HARVEST"):$("#area_status").html("BURNT HARVEST");

            $("#openAreaAreaID").val(areaid);
            $("#openAreaCropiD").val(cropid);
        }
    }

    
}

function completedCropping(id){
    $.post(base_url+"/farms/complete-cropping", {
        id: id
    }, function(res){
        if(res == 1){
            add_success("Cropping for this area has been set to complete");
        }else{
            failed_query();
        }
    })
}

function show(){
    alert("Test")
}
function showDetails(id){
    alert(id)
}
function showModal(slug){
   var route = window.location.origin;

    $('#coords1').attr('src', route+"/sugarcane/"+slug)

    $('#newCanvas').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}
function makeModal() {
    $('#make_modal').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}
function close_make_modal() {
    location.reload();
}

function recepients_data(cropid) {
    $("#croppingHistoryList").DataTable().destroy();
    $('#croppingHistoryList').dataTable({
        "processing": true,
        "ajax": {
            "url": base_url + "/farms/history-list",
            "dataSrc": "data",
            "data": {
                cropid: cropid
            },
            "type": "POST"
        },
        "columns": [{
                "data": "count"
            },
            {
                "data": "fullname"
            }

        ]
    });
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>

