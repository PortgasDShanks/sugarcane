<form method="POST" id='updateDetails'>
    <div class="modal fade" id="cropDetails" tabindex="-1" role="dialog" aria-labelledby="cropDetailsLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cropDetailsLabel"><span class='fa fa-check-circle'></span> Crop Area Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="totalHectareAreaNOC">Total Hectare/s</label>
                            <input readonly type="number" class="form-control" id='totalHectareAreaNOC' name="totalHectareNOC" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="viewDOH">Date Planted</label>
                            <input type="text" class="form-control" id='datepicker1' name="viewDOH">
                        </div>
                        <div class="form-group">
                            <label for="viewPPC">Farm Production Cost/Kg</label>
                            <input name="viewPPC" id='viewPPC' class='form-control' type='number'>
                            <input name="cropID" id='cropID' type='hidden'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Update Details</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>