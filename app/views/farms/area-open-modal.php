<form method="POST" id='openAreaforNew'>
    <div class="modal fade" id="openAreaforNewContent" tabindex="-1" role="dialog" aria-labelledby="openAreaforNewContentLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="openAreaforNewContentLabel"><span class='fa fa-check-circle'></span> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <h4>THIS AREA IS UNDER SOIL HEALING</h4>
                        <h5>Would you like to set this area as OPEN for new planting?</h5>
                        <br>
                        <div class='form-group'>
                            <button class='btn btn-sm btn-success'><i class="icofont icofont-thumbs-up"></i> Yes</button>
                            <button type="button" data-dismiss="modal" class='btn btn-sm btn-danger'><i class="icofont icofont-thumbs-down"></i> No</button>
                        </div>
                       
                            <input name="openAreaCropiD" id='openAreaCropiD' type='hidden'>
                            <input name="openAreaAreaID" id='openAreaAreaID' type='hidden'>
                            
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>