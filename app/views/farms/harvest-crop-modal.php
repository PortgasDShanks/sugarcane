<form method="POST" id='harvestCrop'>
    <div class="modal fade" id="harvestCropContent" tabindex="-1" role="dialog" aria-labelledby="harvestCropContentLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="harvestCropContentLabel"><span class='fa fa-check-circle'></span> This area is ready for harvest!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="totalHectareArea">Total Hectare/s</label>
                            <input readonly type="number" class="form-control" id='totalHectareArea' name="totalHectareNOC">
                        </div>
                        <div class="form-group">
                            <label for="datePlantedDOH">Date Planted</label>
                            <input disabled type="text" class="form-control" id='datepicker2' name="datePlantedDOH">
                        </div>
                        <div class="form-group">
                            <label for="harvestDOH">Date to be Harvested</label>
                            <input disabled type="text" class="form-control" id='harvestDOH' name="harvestDOH">
                        </div>
                        <div class="form-group">
                            <label for="harvestPPC">Farm Production Cost/Kg</label>
                            <input readonly name="harvestPPC" id='harvestPPC' class='form-control' type='number'>
                            <input name="harvestCropiD" id='harvestCropiD' type='hidden'>
                            <input name="harvestAreaID" id='harvestAreaID' type='hidden'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Start Harvesting</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>