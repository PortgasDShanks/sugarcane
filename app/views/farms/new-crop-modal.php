<form method="POST" id='saveCropPlant'>
    <div class="modal fade" id="saveCrop" tabindex="-1" role="dialog" aria-labelledby="saveCropLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="saveCropLabel"><span class='fa fa-check-circle'></span> Plant new Crop</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="numberofcrops">Total Hectare</label>
                            <input type="number"  step=".01" class="form-control" id='totalHectare' name="totalHectare" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="dateofharvest">Date Planted</label>
                            <input type="date" class="form-control" id='datePlanted' name="datePlanted" autocomplete="off">
                        </div>
                      
                        <div class="form-group">
                            <label for="pricePercrop">Farm Production Cost/Kg</label>
                            <input name="pricePercrop" id='pricePercrop' class='form-control' type='number'>
                
                            <input name="areaID" id='areaID' type='hidden'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>