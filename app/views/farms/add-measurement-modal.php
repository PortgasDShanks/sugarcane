<form method="POST" id='addMeasure'>
    <div class="modal fade" id="addMeasureModal" tabindex="-1" role="dialog" aria-labelledby="cropDetailsLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cropDetailsLabel"><span class='fa fa-check-circle'></span> Area in Hectare (Ha)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="viewNOC">Area Hectare (Ha)</label>
                            <input type="number" step=".01" class="form-control" id='hectareArea' name="hectareArea" autocomplete="off">
                            <input type="hidden" id='areaID' name="areaID">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Update Details</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>