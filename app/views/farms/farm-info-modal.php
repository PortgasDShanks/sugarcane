<form method="POST" id='FarmInfoSave'>
    <div class="modal fade" id="farmInfo" tabindex="-1" role="dialog" aria-labelledby="farmInfoLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="farmInfoLabel"><span class='feather icon-bookmark'></span> Add Farm Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <input type="hidden" class="form-control" name='farm_address' id='farm_address'>
                <input type="hidden" class="form-control" name='farm_latitude' id='farm_latitude'>
                <input type="hidden" class="form-control" name='farm_longitude' id='farm_longitude'>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Farm Name</span>
                                <input type="text" class="form-control" name='farm_name'>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Farm Description</span>
                                <textarea name="farm_desc" id="farm_desc" rows="3" class='form-control' style='resize: none'></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Hectare (Ha)</span>
                                <input type="number" step=".01" class="form-control" name='totalHectare'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary btn-sm">Finish Setup</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>