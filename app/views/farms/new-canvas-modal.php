<form method="POST" id='newCanvasSave'>
    <div class="modal fade" id="newCanvas" tabindex="-1" role="dialog" aria-labelledby="newCanvasLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newCanvasLabel"><span class='feather icon-bookmark'></span> Add Area Canvas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                        <div class='row'>
                            <div class='col-sm-12'>
                                <input type="hidden" name="farmID" value='<?=$id?>'>
                                <div class="form-group">
                                    <div class="input-group">
                                    <textarea 
                                        rows=3 
                                        style='display:none' 
                                        id='coords1' 
                                        name="coords1" 
                                        class="canvas-area input-xxlarge" 
                                        disabled 
                                        placeholder="Shape Coordinates"
                                        data-image-url="http://localhost/sugarcane/public/assets/uploads/8WWQ20211104123108.jpg" 
                                       >
                                        </textarea>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                        <!-- data-image-url="http://localhost/sugarcane/public/assets/uploads/QPEU20211102112418.jpg" -->
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary btn-sm">Finish Setup</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>