<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';

?>

<style>

</style>
<div class="row">
    <div class='col-sm-12'>
    <div class="card">
        <div class="card-block">
                <div class="row">
                    <div class='col-sm-4'>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Date From: </span>
                                <input type="date" class="form-control" name='dateFrom' id='dateFrom'>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-4'>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Date To: </span>
                                <input type="date" class="form-control" name='dateTo' id='dateTo'>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-4'>
                        <div class="form-group">
                            <button class='btn btn-sm btn-primary' onclick='generateReport()'> <span class='feather icon-refresh-ccw'></span> Generate</button>
                        </div>
                        
                    </div>
                    <div class='col-sm-12 col-xs-12' style='margin-top: 20px;'>
                        <div id='chart'>
                        
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
</div>
<script>
$(document).ready( function(){
    
});
function generateReport(){
    var from = $("#dateFrom").val();
    var to = $("#dateTo").val();
    $("#chart").html("<span class='fa fa-spin fa-spinner'></span>").css("text-align", "center").css("font-size", "50px");

    $.ajax({
        type: "POST",
        url: base_url + "/reports/expenses-report-column",
        data: {
            from: from,
            to: to
        },
        dataType: "json",
        success: function (data) {
            console.log(data)
            Highcharts.chart("chart", {
            chart: {
                type: 'column'
            },
            title: {
                useHTML: true,
                text: "Expenses Report"
            },
            xAxis: {
                type: 'category',
                title: {
                    text: ""
                }
            },
            yAxis: {
                title: {
                    text: 'Amount (in Peso)'
                }
            },
            plotOptions: {
                column: {
                    dataLabels: {
                    enabled: true
                    },
                    enableMouseTracking: true
                },
                series: {
                    connectNulls: true
                }
            },

            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title} </span>: <b>{point.y:.0f}</b><br/>'
            },

            series: data['series']
            });
        },
        error: function (data) {
                alert(data);
                console.log(data)
            }
    })
}

</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>

