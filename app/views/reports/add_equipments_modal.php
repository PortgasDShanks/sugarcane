<form method="POST" action="<?= route('/supply/save-equipments') ?>" id="add_supply_per_type">
    <div class="modal fade" id="equipments" tabindex="-1" role="dialog" aria-labelledby="equipmentsLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="equipmentsLabel"><span class='fa fa-plus-circle'></span> Add Equipments</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6 id="register-response" style="margin-bottom: 30px;"></h6>
                        <div class="md-form">
                            <input type="text" autocomplete='off' id="equipment_name" name="equipment_name" class="form-control" required>
                            <label for="equipment_name">Name <span style="color:red">*</span></label>
                        </div>

                        <div class="md-form">
                            <textarea name="equipment_desc" id="equipment_desc" class='form-control' style='resize: none'></textarea>
                            <label for="equipment_desc">Description <span style="color:red">*</span></label>
                        </div>

                        <div class="md-form">
                            <input type="number" autocomplete='off' id="equipment_qtty" name="equipment_qtty" class="form-control" required>          
                            <label for="equipment_qtty">Quantity <span style="color:red">*</span></label>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save Changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>