<?php

use App\Core\Auth;
use App\Core\Request;
use App\Core\App;
require __DIR__ . '/../layouts/head.php';

?>

<style>

</style>
<div class="row">
    <div class='col-sm-12'>
    <div class="card">
        <div class="card-block">
                <div class="row">
                    <!-- <div class='col-sm-3'>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Date From: </span>
                                <input type="date" class="form-control" name='dateFrom' id='dateFrom'>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Date To: </span>
                                <input type="date" class="form-control" name='dateTo' id='dateTo'>
                            </div>
                        </div>
                    </div> -->
                    <div class='col-sm-4'>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Farm</span>
                                <select name="farmID" class="form-control" id="farmID">
                                    <option value="">&mdash; Please Choose Farm &mdash;</option>
                                    <?php 
                                        
                                        foreach($farms as $farm){
                                    ?>
                                    <option value="<?=$farm['id']?>"><?=$farm['farm_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <button class='btn btn-sm btn-primary' onclick='generateReport()'> <span class='feather icon-refresh-ccw'></span> Generate</button>
                        </div>
                        
                    </div>
                    <div class='col-sm-12 col-xs-12' style='margin-top: 20px;'>
                        <div id='chart'>
                        
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
</div>
<script>
$(document).ready( function(){
    
});
function generateReport(){
    // var from = $("#dateFrom").val();
    // var to = $("#dateTo").val();
    var farmID = $("#farmID").val();
    //alert(farmID)
    $("#chart").html("<span class='fa fa-spin fa-spinner'></span>").css("text-align", "center").css("font-size", "50px");

    $.ajax({
        type: "POST",
        url: base_url + "/reports/area-report-pie",
        data: {
            farmID: farmID
        },
        dataType: "json",
        success: function (data) {
            console.log(data)
            Highcharts.chart("chart", {
            chart: {
                type: 'pie'
            },
            title: {
                useHTML: true,
                text: "Statistics Report"
            },
            xAxis: {
                type: 'category',
                title: {
                    text: "Sales Per Month"
                }
            },
            yAxis: {
                title: {
                    text: 'Amount (in Peso)'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                },
                series: {
                    connectNulls: true
                }
            },

            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title} </span>: <b>{point.y:.0f} Hectare</b><br/>'
            },

            series: data['series']
            });
        },
        error: function (data) {
                alert(data);
                console.log(data)
            }
    })
}

</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>

