<?php 
use App\Core\Auth;
use App\Core\Request;

require 'layouts/head.php';     
    
?>
<style>
    .welcome-msg {
        margin-top: 10%;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="d-flex flex-column align-items-center justify-content-center">
            <h2 class="mb-0 text-muted welcome-msg">Welcome to GIS Crop Mapping</h2>
            <!-- <p class="text-muted">We can't wait to see what you build</p> -->
        </div>
    </div>
</div>    
<script>
    $('img').mapster({
    fillOpacity: 0.5,
    render_highlight: {
        fillColor: '2aff00',
        stroke: true,
        altImage: '<?=public_url('/assets/uploads/QPEU20211102112418.jpg')?>'
    },
    render_select: {
        fillColor: 'ff000c',
        stroke: false,
        altImage: '<?=public_url('/assets/uploads/QPEU20211102112418.jpg')?>'
    },
    fadeInterval: 50,
    mapKey: 'data-state',
    areas: [
    {
        key: 'TX',
        selected: true
    },
    {
        key: 'ME',
        selected: true
    },
    {
        key: 'WA',
        staticState: false
    },
    {
        key: 'OR',
        isSelectable: false
    }]
});
</script>
<?php require 'layouts/footer.php'; ?>

