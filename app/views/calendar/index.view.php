<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';

?>

<style>

</style>
<div class="row">
    <div class='col-sm-12'>
    <div class="card">
        <div class="card-block">
            <div class="">
                <div class="row">
                    <!-- <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary' onclick='showModal()' style='float: right'> <span class='feather icon-plus-circle'></span> Add Event</button>
                    </div> -->
                    <div class='col-sm-4 col-xs-12' style=''>
                      
                        <div class="form-group">
                            <select name="farmList" id="farmList" class='form-control'>
                                <option value="">&mdash; Please Select Farm &mdash;</option>
                                <?php 
                                    foreach ($farms as $farm) {
                                        echo "<option value='".$farm['id']."'>".$farm['farm_name']."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                     
                    </div>
                    <div class='col-sm-4 col-xs-12' style=''>
                      
                        <div class="form-group">
                            
                            <select name="CropYear" id="CropYear" class='form-control'>
                                <option value="">&mdash; Please Select Year &mdash;</option>
                                <?php 
                                    $year_start = 2010;
                                    $year_end = date("Y");
                                    for ($i=$year_start; $i <= $year_end; $i++) { 
                                        echo "<option value='".$i."'>".$i."</option>";
                                    } 
                                        
                                    
                                ?>
                            </select>
                        </div>
                     
                    </div>
                    <div class='col-sm-4 col-xs-12' style=''>
                      
                        <div class="form-group">
                           <button class='btn btn-sm btn-success' onclick='getFarmCropping()'><span class='feather icon-eye'></span> View</button>
                        </div>
                     
                    </div>
                    <div class='col-sm-12' id='cropping-view'>
                        <!-- <table class='table table-bordered'>
                                <thead>
                                    <tr>
                                        <th>Year</th>
                                        <th colspan='12'>Month</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>addslashes</td>
                                        <td>asdasad</td>
                                        <td>asdasd</td>
                                        <td>asd</td>
                                    </tr>

                                </tbody>
                        </table> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<?php require __DIR__ . '/add-event-modal.php'; ?>
<?php require __DIR__ . '/update-event-modal.php'; ?>
<script>
    function getFarmCropping(){
        var farm = $("#farmList").val();
        var year = $("#CropYear").val();
        $.post(base_url+"/calendar/active-cropping-data", {
            farm: farm,
            year: year
        }, function(res){
            $("#cropping-view").html(res);
        });
    }
$(document).ready( function(){
    
    $('#calendar').fullCalendar({
        locale: 'es',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay,listYear,year'
      },
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
        events: 'https://fullcalendar.io/api/demo-feeds/events.json',
      eventConstraint: {
          start: moment().format('YYYY-MM-DD'),
          end: '2100-01-01'
      }
    });
});
function deleteEvent(id){
    $.post(base_url+"/calendar/delete-event", {
        id: id
    }, function(res){
        if(res > 0){
            delete_success("Event was successfully deleted!");
        }else{
            failed_query();
        }
    });
}
function updateEvent(resized, id){
    $.post(base_url+"/calendar/resize-event", {
        resized: resized,
        id: id
    }, function(res){
        if(res > 0){
            update_success("Event was successfully resized!");
        }else{
            failed_query();
        }
    })
}

function dropEvent(new_s, new_e, id){
    $.post(base_url+"/calendar/drop-event", {
        new_s: new_s,
        new_e: new_e,
        id: id
    }, function(res){
        if(res > 0){
            update_success("Event was successfully moved!");
        }else{
            failed_query();
        }
    })
}
function showModal(){
    $("#addEvents").modal({
        show: true
    })
}

</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>

