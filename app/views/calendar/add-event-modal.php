<form method="POST" id='saveEvents'>
    <div class="modal fade" id="addEvents" tabindex="-1" role="dialog" aria-labelledby="addEventsLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addEventsLabel"><span class='feather icon-user-plus'></span> Add Doctor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Event Name</span>
                                <input type="text" class="form-control" name='eventName'>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Event Description</span>
                                <textarea name="desc" class='form-control' style='resize: none' rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon2">Event Date</span>
                                <input type="date" class="form-control" id='eventFrom' name='eventFrom'>
                                <input type="date" class="form-control" id='eventTo' name='eventTo'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary btn-sm">Save changes</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>