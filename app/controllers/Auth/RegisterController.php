<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;

class RegisterController
{
    protected $pageTitle;

    public function index()
    {
        Auth::isAuthenticated();

        $pageTitle = "Register";
        return view('/auth/register', compact('pageTitle'));
    }

    public function store()
    {
        $request = Request::validate('/register', [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);


        $rand_color = "#".substr(md5(rand()), 0, 6);

            $user = [
                'email' => $request['email'],
                'company_name' => $request['name'],
                'username' => $request['email'],
                'password' => bcrypt($request['password']),
                'role_id' => 'C',
                'color' => $rand_color
            ]; 

           $user = DB()->insert("users", $user, "Y");
            // if($user > 0){
            //     $content = " New Company Registration from ". $request['company_name'] .". Click for more details";
            //     $notif_data = [
            //         "notif_from" => $user,
            //         "notif_to" => 1,
            //         "notif_type" => 0,
            //         "notif_content" => $content,
            //         "notif_status" => 0,
            //         "transaction_id" => 0
            //     ];

            // $notif = DB()->insert("notif", $notif_data);
            // }
            
        
            redirect('/register', ["message" => "Registration Submitted! <br> An Email will be sent to you regarding on your registration status.", "status" => "success"]);
    }

}
