<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class ReportController
{
    protected $pageTitle;

    public function areaReport()
    {
        $pageTitle = "Statistics";

        $auth = Auth::user('id');

        $farms = DB()->selectLoop("*", "farms", "company_id = '$auth'")->get();

        return view('/reports/area-report', compact('pageTitle', 'farms'));
    }

    public function expensesReport()
    {
        $pageTitle = "Expenses";

        return view('/reports/expenses', compact('pageTitle'));
    }

    public function cropsReport()
    {
        $pageTitle = "Crops Planted";

        return view('/reports/crops', compact('pageTitle'));
    }

    public function areaReportPie()
    {
        $request = Request::validate();
        $date = date("Y-m-d");
        $header['series'] = array();
        $data['name'] = "Months";
        $data['colorByPoint'] = true;
        $data['data'] = array();

        $content_names = [
            "To be harvest" => 0,
            "Ongoing Harvest" => 1,
            "Unused Land/Other Crop" => 2
        ];

        $farm_hc = DB()->select("*", "farms", "id='$request[farmID]'")->get();

        foreach ($content_names as $names => $key) {
            $list = array();
            $list['name'] = $names; 
            $list['title'] = $names; 
            
            $hect = 0;
            switch ($key) {
                case 0:
                    $crop = DB()->selectLoop("*", "crop_area AS ca, crops as c", "farm_id = '$request[farmID]' AND ca.id = c.area_id AND completed_status = 0 AND c.status = 1")->get();

                    
                    foreach ($crop as $crop_details) {
                        $harvest_date = ($crop_details['date_harvested'] == NULL || $crop_details['date_harvested'] == '0000-00-00' || $crop_details['date_harvested'] == '')?$crop_details['date_planted']:$crop_details['date_harvested'];

                       if($date > $harvest_date){
                            $hect += $crop_details['hectare'];
                       }else{
                            $hect = $crop_details['hectare'];
                       }
                    }
                    $list['color'] = "#A4DE02"; 
                    $total = floatVal($hect);
                    //$perc = ($hect / $farm_hc['hectare']) * 100;
                    
                    break;
                case 1:
                    $crop = DB()->select("sum(hectare) as t", "crop_area AS ca, crops as c", "farm_id = '$request[farmID]' AND ca.id = c.area_id AND completed_status = 0 AND c.status = 2")->get();

                    $hect = $crop['t'];
                    $list['color'] = "#4DED30"; 
                    $total = $hect;
                    break;
                default:
                    $crop = DB()->select("sum(hectare) as t", "crop_area AS ca, crops as c", "farm_id = '$request[farmID]' AND ca.id = c.area_id")->get();

                    $hect = $farm_hc['hectare'] - $crop['t'];
                    $list['color'] = "#C1C1C1"; 
                    $total = $hect;
                    break;
            }

            

            $list['y'] = $total;
            
            array_push($data['data'], $list);
        }
        array_push($header['series'], $data);
		echo json_encode($header);
    }

    public function expensesReportColumn()
    {
        $request = Request::validate();
        $user = Auth::user('id');
        $header['series'] = array();
        $data['name'] = "Farms";
        $data['colorByPoint'] = true;
        $data['data'] = array();

        $farms = DB()->selectLoop("*", "farms", "company_id = '$user'")->get();

        foreach ($farms as $names) {
            $list = array();
            $list['name'] = $names['farm_name']; 
            $list['title'] = $names['farm_name']; 

            $expense = DB()->select("SUM(c.number_of_crops * c.price_per_crop) as total", "crop_area as ca , crops as c", "ca.id = c.area_id AND ca.farm_id = '$names[id]' AND c.date_added BETWEEN '$request[from]' AND '$request[to]'")->get();

            if(empty($expense['total'])){
                $total = 0;
            }else{	
                $total = $expense['total'] * 1;
            }

            $list['y'] = $total;
            array_push($data['data'], $list);
        }
        array_push($header['series'], $data);
		echo json_encode($header);
    }

    public function cropsReportColumn()
    {
        $request = Request::validate();
        $user = Auth::user('id');
        $header['series'] = array();
        $data['name'] = "Farms";
        $data['colorByPoint'] = true;
        $data['data'] = array();

        $farms = DB()->selectLoop("*", "farms", "company_id = '$user'")->get();

        foreach ($farms as $names) {
            $list = array();
            $list['name'] = $names['farm_name']; 
            $list['title'] = $names['farm_name']; 

            $expense = DB()->select("SUM(c.number_of_crops) as total", "crop_area as ca , crops as c", "ca.id = c.area_id AND ca.farm_id = '$names[id]' AND c.date_added BETWEEN '$request[from]' AND '$request[to]'")->get();

            if(empty($expense['total'])){
                $total = 0;
            }else{	
                $total = $expense['total'] * 1;
            }

            $list['y'] = $total;
            array_push($data['data'], $list);
        }
        array_push($header['series'], $data);
		echo json_encode($header);
    }
   
}
