<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;

class WelcomeController
{
    protected $pageTitle;

    public function home()
    {
        $pageTitle = "Home";

        // $overall = DB()->select("sum(f.hectare) as t", "farms as f, users as u", "t.company_id = u.user_id")->get();
        // $hect = $overall['t'];
        return view('/home', compact('pageTitle'));
    }

    public function update()
    {
        $request = Request::validate();

        $data = [
            "notif_status" => 1
        ];

        $update = DB()->update("notif", $data, "id = '$request[id]'");

        echo $update;
    }
}
