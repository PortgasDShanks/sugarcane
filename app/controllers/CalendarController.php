<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class CalendarController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Cropping Calendar";

        $company = Auth::user('id');
        $farms = DB()->selectLoop("*","farms","company_id = '$company'")->get();

        return view('/calendar/index', compact('pageTitle',"farms"));
    }
    public function store()
    {
        $request = Request::validate('', [
            "eventName" => ['required'],
            "desc" => ['required'],
            "eventFrom" => ['required'],
            "eventTo" => ['required']
        ]);

        $data = [
            "event_name" => $request['eventName'],
            "event_desc" => $request['desc'],
            "event_date_from" => $request['eventFrom'],
            "event_date_to" => $request['eventTo'],
            "user_id" => Auth::user('id')
        ];

        $response = DB()->insert("events", $data);
       

        echo $response;
    }

    public function delete()
    {
        $request = Request::validate('');

        $response = DB()->delete("events", "id = '$request[id]'");

        echo $response;
    }

    public function resize()
    {
        $request = Request::validate('');

        $new_end_date = date("Y-m-d" , strtotime("-1 day", strtotime($request['resized'])));
        $update_data = [
            "event_date_to" => $new_end_date
        ];

        $response = DB()->update('events', $update_data, "id = '$request[id]'");

        echo $response;
    }

    public function update()
    {
        $request = Request::validate('');

        $update_data = [
            "event_name" => $request['show_eventName'],
            "event_desc" => $request['show_desc']
        ];
        
        $response = DB()->update('events', $update_data, "id = '$request[eventID]'");

        echo $response;
    }

    public function drop()
    {
        $request = Request::validate('');
        $new_end = date("Y-m-d", strtotime("-1 day", strtotime($request['new_e'])));
        $update_data = [
            "event_date_from" => $request['new_s'],
            "event_date_to" => $new_end
        ];
        
        $response = DB()->update('events', $update_data, "id = '$request[id]'");

        echo $response;
    }

    public function croppingData()
    {
        $request = Request::validate();
        $data = "";
        $year = $request['year'];
        $farms = DB()->select("f.id as farmid, c.date_planted","farms as f, crop_area as ca, crops as c", "f.id = ca.farm_id AND ca.id = c.area_id AND f.id = '$request[farm]' AND c.completed_status = 0")->get();

        if($farms){
            $data .= "<table class='table table-bordered'>";
                $data .= "<thead>";
                $data .= "<tr><th colspan='12' style='text-align: center'>".$year."</th></tr>";
                $data .= "<tr>"; 
                    $months = array_months();
                    foreach($months as $month => $num){
                        
                            $data .= "<th style='text-align: center'>".$month."</th>";
                       
                    }
                    $data .= "<tr>";      
                    $data .= "<tr>";
                    foreach($months as $month => $num){
                            $yrmnth = $year."".$num;
                            $date_planted_harvest = date("Ym", strtotime("+10 months", strtotime($farms['date_planted'])));
                            $date_planted = date("Ym", strtotime($farms['date_planted']));
                            $color = ($yrmnth >= $date_planted && $yrmnth < $date_planted_harvest)?"#4DED30":"";
                            $data .= "<th style='background-color: ".$color."'></th>";
                    
                    }
                    $data .= "</tr>";
                $data .= "</thead>";
            $data .= "</table>";
        }else{
            $data .= "<span class='d-flex flex-column align-items-center justify-content-center' style='font-size: 40px;color: #ccc;'>No active cropping available</span>";
        }

        echo $data;
    }

    
}
