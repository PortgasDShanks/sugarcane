<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class CompanyController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Companies";

       

        return view('/company/index', compact('pageTitle'));
    }

    public function search()
    {
        $request = Request::validate('');
        $where_add = ($request['searchval'] == '')?"":" AND company_name LIKE '%$request[searchval]%'";
        $companies = DB()->selectLoop("*","users", "role_id = 'C' $where_add")->get();
        $data = "";
        if(count($companies) > 0){
            
            foreach ($companies as $company) {
              
           
            $data .= "<div class='col-xs-12 col-sm-4 '>";
               $data .= '<div class="card ch-padd-hover z-depth-right-0" style="border: 1px solid rgb(0 0 0 / 26%);overflow: auto;min-height: 150px;">';
                    $data .= '<div class="card-body">';
                        $data .= '<span class="d-flex flex-row show-onhover">';
                        $data .= '<i class="feather icon-settings show-onhover text-muted" style="font-size: 14px;cursor: pointer;" onclick="showOptions(\''.$company['id'].'\',\''.$company['status'].'\',\''.$company['company_name'].'\')"></i>';
                        $data .= '</span>';
                        $data .= '<p class="card-text d-flex flex-column" style="font-size: 18px;">';
                            $data .= $company['company_name'];
                            $data .= '<hr>';
                             $data .= "<div class='text-muted' style='text-align: center'>";
                                $data .= ($company['status'] == 0)?"<span class='label label-warning'>Pending Application</span>":(($company['status'] == 1)?"<span class='label label-success'>Active Company</span>":(($company['status'] == 2)?"<span class='label label-info'>Inactive Company</span>":"<span class='label label-danger'>Cancelled Application</span>"));
                             $data .= '</div>';
                        $data .= '</p>';
                    $data .= '</div>';
                $data .= '</div>';
             $data .= '</div>';
            }
        }else{
            $data .= '<div class="col-sm-12">';
                $data .= '<div class="d-flex flex-column align-items-center justify-content-center">';
                    $data .= '<h2 class="mb-0 text-muted">No Company is registered yet!</h2>';
                        
                $data .= '</div>';
            $data .= '</div>';
        }

        echo $data;
    }

    public function changeStatus()
    {
        $request = Request::validate();

        $stat = ($request['status'] == 1)?1:(($request['status'] == 2)?2:(($request['status'] == 3)?3:1));

        $data = [
            "status" => $stat
        ];

        $response = DB()->update('users', $data, "id = '$request[id]'");
        if($response > 0){
            $company = DB()->select("*","users","id = '$request[id]'")->get();

            $content = ($request['status'] == 1)?"<strong>Congratulations!</strong> Your application is now approved by the management.":(($request['status'] == 2)?"We're sorry to inform you that your account in Sugarcane Production System was set to Inactive. Please Contact the administrator for more information":(($request['status'] == 4)?"<strong>Congratulations!</strong> Your account in Sugarcane Production System is now active. You can now continue managing your farms":"We're sorry to inform you that your application was rejected by the administration. Please contact the administrator for more information"));

            $email = $this->sendEmail($company['email'], $company['company_name'], $content);
        }

        echo $email;
    }

    public function sendEmail($email_address, $company_name, $content)
    {
        $subject = "Sugarcane Production System Account Status";

        $body = "<!DOCTYPE html>
                    <html>
                        <head>
                            <title></title>
                            <link rel='stylesheet' href='".public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css')."'>
                        </head>
                        <body><h1>Hi ".ucwords($company_name).",</h1>
                    <p>".$content."</p>
                    
                    <p>Best Regards,<br />Management Team</p>
                    Please do not reply.
                        </body>
                    </html>";

        $recepients = $email_address;

        $sent = sendMail($subject, $body, $recepients);

        return $sent;
    }
}
