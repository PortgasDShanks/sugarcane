<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class FarmController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Farms In Map";
        $id = Auth::user('id');
        

        return view('/farms/index', compact('pageTitle'));
    }

    public function newFarm()
    {
        $pageTitle = "Add New Farm";

       

        return view('/farms/new-farm', compact('pageTitle'));
    }

    public function view($id)
    {
        $pageTitle = "Farm Details";
        $auth = Auth::user('id');
        $checkSlug = DB()->select("*", "user_uploads", "user_id = '$auth' AND upload_category = 'F' AND farm_id = '$id'")->get();

        $coord_area = DB()->query("SELECT c.id as areaid, c.farm_id, cc.id as cropid, cc.status as ccstat, c.open_close as cstat, cc.date_harvested, c.coordinates, c.map_key, cc.date_planted, cc.completed_status FROM `crop_area` as c LEFT JOIN `crops` as cc ON c.id = cc.area_id WHERE c.farm_id = '$id'", "Y")->get();

        $slug = (empty($checkSlug['slug']))?"":$checkSlug['slug'];

        return view('/farms/view-farm', compact('pageTitle', 'id', 'slug', 'coord_area'));
    }

    public function new($id)
    {
        $pageTitle = "Add New Canvas on Area";
        $auth = Auth::user('id');
        $checkSlug = DB()->select("*", "user_uploads", "user_id = '$auth' AND upload_category = 'F' AND farm_id = '$id'")->get();
        $slug = $checkSlug['slug'];
        return view('/farms/new-canvas', compact('pageTitle', 'id', 'slug'));
    }

    public function cropDetails()
    {
        $request = Request::validate();

        $crop = DB()->select("*", "crops", "id = '$request[cropid]'")->get();

        $response = [];

        $response['number_of_crops'] = $crop['number_of_crops'];
        $response['hectares'] = $crop['total_hc'];
        $response['date_to_be_harvested'] = date("m/d/Y", strtotime("+10 months", strtotime($crop['date_planted'])));
        $response['date_planted'] = date("m/d/Y", strtotime($crop['date_planted']));
        $response['price_per_crop'] = $crop['price_per_crop'];

        echo json_encode($response);
    }

    public function store()
    {
        $auth = Auth::user('id');
        $request = Request::validate();

        $data = [
            "company_id" => $auth,
            "farm_name" => $request['farm_name'],
            "farm_desc" => $request['farm_desc'],
            "farm_address" => $request['farm_address'],
            "latitude" => $request['farm_latitude'],
            "longitude" => $request['farm_longitude'],
            "hectare" => $request['totalHectare'],
            "status" => 0
        ];


        $response = DB()->insert("farms", $data);

        echo $response;
    }

    public function storeFile($id)
    {
        $user_id = Auth::user('id');
        if(Request::hasFile('upload_file')){
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $this->saveFilesInDb($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
            echo $folder;
        }
    }   

    public function saveFilesInDb($user_id, $path, $file_name, $fileSize, $fileType, $previewSize, $id)
    {
      

        $data_form = [
            "user_id" => $user_id,
            "slug" => $path,
            "filetype" => $fileType,
            "filename" => $file_name,
            "filesize" => $fileSize,
            "iconsize" => $previewSize,
            "upload_category" => "F",
            "farm_id" => $id
        ];

        DB()->insert("user_uploads", $data_form);
    }

    public function storeArea()
    {
        $request = Request::validate();

        $data = [
            "farm_id" => $request['farmID'],
            "coordinates" => $request['coords1'],
            "map_key" => getRandonChars()
        ];

        $response = DB()->insert("crop_area", $data, "Y");

        echo $response;
    }

    public function storeMeasureArea()
    {
        $request = Request::validate();

        $data = [
            "hectare" => $request['hectareArea']
        ];

        $response = DB()->update("crop_area", $data, "id = '$request[areaID]'");

        echo $response;
    }

    public function storeCrop()
    {
        $request = Request::validate();

        $refCode = "ref-".date("Ymd");

        $data = [
            "area_id" => $request['areaID'],
            "ref_code" => $refCode,
            "total_hc" => $request['totalHectare'],
            "date_planted" => $request['datePlanted'],
            "number_of_crops" => 0,
            "price_per_crop" => $request['pricePercrop'],
            "status" => 1
        ];

        $response = DB()->insert("crops", $data);
        if($response > 0){
            $close = [
                "date_close" => date('Y-m-d'),
                "open_close" => 1
            ];

            $update = DB()->update("crop_area", $close, "id = '$request[areaID]'");
        }

        echo $response;
    }

    public function cropUpdate()
    {
        $request = Request::validate();
        $newData = date("Y-m-d", strtotime( $request['viewDOH']));

        $data = [
            "number_of_crops" => $request['viewNOC'],
            "date_planted" => $newData,
            "price_per_crop" => $request['viewPPC']
        ];

        $response = DB()->update("crops", $data, "id = '$request[cropID]'");

        echo $response;
    }

    public function harvestCrop()
    {
        $request = Request::validate();

        $data = [
            "status" => 2
        ];

        $crop = DB()->update("crops", $data, "id='$request[harvestCropiD]'");
        // if($crop > 0){
        //     $open = [
        //         "open_close" => 0
        //     ];

        //     $open_area = DB()->update("crop_area", $open, "id='$request[harvestAreaID]'");
        // }

        echo $crop;
    }

    public function finishHarvest()
    {
        $request = Request::validate();
        $date = date("Y-m-d");

        if($request['completeHarvest'] == 0){
            $data = [
                "date_harvested" => $date,
                "status" => 1,
            ];
        }else{
            $data = [
                "date_harvested" => $date,
                "status" => 1,
                "completed_status" => 1
            ];
        }
        

        $crop = DB()->update("crops", $data, "id='$request[finishharvestCropiD]'");

        $cropping_data = [
            "crop_id" => $request['finishharvestCropiD'],
            "status" => $request['harvestType']
        ];

        $cropping_history = DB()->insert("cropping", $cropping_data);
        echo $crop;
    }

    public function openAreafornewPlant()
    {
        $request = Request::validate();

        $data = [
            "date_open" => date('Y-m-d'),
            "open_close" => 0
        ];

        $crop = DB()->update("crop_area", $data, "id='$request[openAreaAreaID]'");
         if($crop > 0){
            $open = [
                "finished_status" => 1
            ];

            $open_area = DB()->update("crops", $open, "id='$request[openAreaCropiD]'");
        }

        echo $crop;
    }

    public function countCropping()
    {
        $request = Request::validate();

        $count = DB()->select("count(*) as c","cropping","crop_id = '$request[cropid]'")->get();

        echo $count['c'];
    }


    function historyList()
    {
        $request = Request::validate();

        $msgs = DB()->selectLoop("*", "cropping", "crop_id = '$request[cropid]' ORDER BY date_updated DESC")->get();

        $count = 1;
        $response['data'] = [];

        if (count($msgs) > 0) {
            $list = [];
            foreach ($msgs as $msg) {
                $list['sms_id'] = $msg['id'];
                $list['count'] = date("F d, Y", strtotime($msg['date_updated']));
                $list['fullname'] = ($msg['status'] == 3)?"GREEN HARVEST":"BURNT HARVEST";

                array_push($response['data'], $list);
            }
        }
        echo json_encode($response);
    
    }

}
