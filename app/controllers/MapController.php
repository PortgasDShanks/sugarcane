<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class MapController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Map";

        $company = DB()->selectLoop("*", "users", "role_id = 'C'")->get();

        return view('/map/index', compact('pageTitle', 'company'));
    }

    public function coords()
    {
        $request = Request::validate();
        $id = $request['checkedelemids'];
        $farms = DB()->selectLoop("*", "farms", "company_id IN ($id)")->get();

        $response['data'] = array();
        $list = [];
        foreach ($farms as $farm) {
            $list = array();
            $list['name'] = $farm["farm_name"];
            $list['lat'] = $farm["latitude"];
            $list['lng'] = $farm["longitude"];
            array_push($response['data'], $list);
            
        }
        echo json_encode($response);
    }

}
